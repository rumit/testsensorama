﻿using System;
using System.Collections;
using UnityEngine;

public class CoroutineHelper : MonoBehaviour
{
    private static CoroutineHelper _instance;

    private static CoroutineHelper Instance
    {
        get
        {
            if (_instance) 
                return _instance;

            var go = new GameObject {name = "CoroutineHelper", hideFlags = HideFlags.HideAndDontSave};
            DontDestroyOnLoad(go);
            _instance = go.AddComponent<CoroutineHelper>();
            return _instance;
        }
    }

    public static Coroutine LaunchCoroutineWithCompletion(IEnumerator coroutine, System.Action onCompletedAction,
        bool callOnCompletedEvenExceptionOccurs = true)
    {
        return Instance.StartCoroutine(DoLaunchCoroutineWithCompletion(coroutine, onCompletedAction, callOnCompletedEvenExceptionOccurs));
    }

    public static Coroutine LaunchCoroutine(IEnumerator coroutine)
    {
        return Instance.StartCoroutine(coroutine);
    }

    public static void CancelCoroutine(Coroutine coroutine)
    {
        if (coroutine != null)
            Instance.StopCoroutine(coroutine);
    }

    private static IEnumerator DoLaunchCoroutineWithCompletion(IEnumerator coroutine, System.Action onCompletedAction,
        bool callOnCompletedEvenExceptionOccurs = true)
    {
        while (true)
        {
            object current;
            try
            {
                if (!coroutine.MoveNext())
                {
                    break;
                }
                current = coroutine.Current;
            }
            catch (Exception ex)
            {
                if(callOnCompletedEvenExceptionOccurs) 
                {
                    onCompletedAction?.Invoke();
                }
                Debug.LogErrorFormat("CoroutineHelper catch exception and throw it upper. OnCompleted action {0} called. Ex: {1}.",
                      callOnCompletedEvenExceptionOccurs ? "was" : "wasn't", ex);
                throw ex;
            }
            yield return current;
        }
        onCompletedAction?.Invoke();
    }
}
