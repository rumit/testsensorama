﻿using System;
using UnityEngine;

public class GeomUtil
{
    public static bool RectIntersectsCircle(Vector2 rectCenter, Vector2 rectHalfSize, Vector2 circleCenter, float circleRadiusSquared)
    {
        // shift coordinate system to rectangle center
        var diff = rectCenter - circleCenter;
        // fold circle into positive quadrant
        var diffPositive = new Vector2(Math.Abs(diff.x), Math.Abs(diff.y));
        // shift coordinate system to rectangle corner
        var closest = diffPositive - rectHalfSize;
        // set negative coordinates to 0 so they do not affect the check below
        var closestPositive = new Vector2(Math.Max(closest.x, 0), Math.Max(closest.y, 0));
        // perform distance check
        return closestPositive.sqrMagnitude <= circleRadiusSquared;
    }
    
    public static bool LineIntersectsRect(Vector2 a, Vector2 b, Rect r)
    {
        var minX = Math.Min(a.x, b.x);
        var maxX = Math.Max(a.x, b.x);
        var minY = Math.Min(a.y, b.y);
        var maxY = Math.Max(a.y, b.y);

        if (r.xMin > maxX || r.xMax < minX)
            return false;

        if (r.yMin > maxY || r.yMax < minY)
            return false;

        if (r.xMin < minX && maxX < r.xMax) 
            return true;

        if (r.yMin < minY && maxY < r.yMax) 
            return true;

        float YForX(float x) => a.y - (x - a.x) * ((a.y - b.y) / (b.x - a.x));

        var yAtRectLeft = YForX(r.xMin);
        var yAtRectRight = YForX(r.xMax);

        if (r.yMax < yAtRectLeft && r.yMax < yAtRectRight)
            return false;

        return !(r.yMin > yAtRectLeft) || !(r.yMin > yAtRectRight);
    }
    
    public static Vector3 GetTangentToCircle(Vector3 center, float radius, int angle, float length)
    {
        var pt = GetPointOnCircle(center, radius, angle);
        return Vector3.Cross(Vector3.up, pt - center).normalized * length;
    }
    
    public static  Vector3 GetTangentToCircle(Vector3 point, Vector3 center, float length)
    {
        return Vector3.Cross(Vector3.up, point - center).normalized * length;
    }
    
    public static  Vector3 GetPointOnCircle(Vector3 center, float radius, int angle)
    {
        var radians = angle * Mathf.Deg2Rad;
        return center + new Vector3(Mathf.Cos(radians) * radius, 0, Mathf.Sin(radians) * radius);
    }
    
    public static Vector2 GetPointOnCircle(Vector2 center, float radius, float angle)
    {
        var radians = angle * Mathf.Deg2Rad;
        return center + new Vector2(Mathf.Cos(radians) * radius, Mathf.Sin(radians) * radius);
    }
    
    public class Bezier
    {
        public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            t = Mathf.Clamp01(t);
            var oneMinusT = 1f - t;
            return
                oneMinusT * oneMinusT * oneMinusT * p0 +
                3f * oneMinusT * oneMinusT * t * p1 +
                3f * oneMinusT * t * t * p2 +
                t * t * t * p3;
        }
        
        public static Vector3[] GetPath(int resolution, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            var result = new Vector3[resolution];
            for (var i = 0; i < resolution; i++)
            {
                var step = (float) i / resolution;
                var p = GetPoint(p0, p1, p2, p3, step);
                result[i] = p;
            }
            return result;
        }
    }
}
