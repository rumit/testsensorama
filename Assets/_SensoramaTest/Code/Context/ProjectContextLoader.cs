﻿using UnityEngine;

namespace SensoramaTest
{
    [DisallowMultipleComponent]
    public class ProjectContextLoader : MonobehaviourSingleton<ProjectContext>
    {
        private static ProjectContext _instance;
        
        [SerializeField] private BattleSceneContext _battleSceneContext;

        private void Awake()
        {
            if(_instance != null)
                return;
            
            var contextAsset = Resources.Load<ProjectContext>("ProjectContext");
            _instance = Instantiate(contextAsset);
            _instance.Init(_battleSceneContext);
        }
    }
}