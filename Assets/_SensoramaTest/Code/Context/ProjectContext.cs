﻿using SensoramaTest.Battle;
using SensoramaTest.Battle.Configs;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SensoramaTest
{
    public class ProjectContext : MonoBehaviour
    {
        [SerializeField] private BattleConfig _battleConfig;
        public BattleConfig BattleConfig => _battleConfig;

        public BattleSceneContext BattleSceneContext { get; private set; }
        public ARStateController ARStateController { get; private set; }
        public BattleUpdateSystem BattleUpdateSystem { get; private set; }
        public BattleInputSystem BattleInputSystem { get; private set; }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void Init(BattleSceneContext context)
        {
            BattleSceneContext = context;
            
            ARStateController = new ARStateController(this);
            BattleUpdateSystem = gameObject.AddComponent<BattleUpdateSystem>()
                .Begin(this);
            BattleInputSystem = gameObject.AddComponent<BattleInputSystem>()
                .Init(this);
            
            new BattleInitializer(this);
        }
    }
}