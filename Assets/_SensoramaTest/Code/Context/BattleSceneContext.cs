﻿using System;
using SensoramaTest.Battle;
using SensoramaTest.Battle.Enemies;
using SensoramaTest.Battle.Turret;
using SensoramaTest.Battle.UI;
using TMPro;
using UnityEngine;

namespace SensoramaTest
{
    [Serializable]
    public class BattleSceneContext
    {
        [Header("UI")]
        public UIController Ui;
        public TextMeshProUGUI TurretHealthField;
        public TextMeshProUGUI TurretScoreField;

        [Space(10)]
        public DefaultObserverEventHandler ArObserver;
        public BattleContainer BattleContainer;
        public Turret Turret;
        
        [Space(10)]
        public Camera Camera;
        
        [NonSerialized]public EnemiesSpawner EnemiesSpawner;
        [NonSerialized]public TargetsManager TargetsManager;
    }
}