﻿using System;
using System.Globalization;
using SensoramaTest.Battle.Turret;
using TMPro;
using UnityEngine;

namespace SensoramaTest.Battle.UI
{
    public class UIController : MonoBehaviour
    {
        private ProjectContext _context;

        private Turret.Turret _turret;

        private TextMeshProUGUI _turretHealthField;
        private TextMeshProUGUI _turretScoreField;
        private BattleOverPopup _battleOverPopupRef;
        private Action _battleFinishCallback;

        private void OnDestroy()
        {
            if (_turret == null)
                return;
            
            _turret.Model.GetDamage -= OnTurretGetDamage;
            _turret.Model.GetScore -= OnTurretGetScore;
        }

        public void Init(ProjectContext context)
        {
            _context = context;
            var battleContext = _context.BattleSceneContext;
            _battleOverPopupRef = _context.BattleConfig.BattleOverPopuRef;

            _turretHealthField = battleContext.TurretHealthField;
            _turretScoreField = battleContext.TurretScoreField;
            
            _turret = battleContext.Turret;
            ResetTurretData();

            _turret.Model.GetDamage += OnTurretGetDamage;
            _turret.Model.GetScore += OnTurretGetScore;
        }

        public void ResetTurretData()
        {
            _turretHealthField.text = _turret.Model.Health.ToString(CultureInfo.InvariantCulture);
            _turretScoreField.text = "0";
        }

        public void DisplayBattleOver(Action callback)
        {
            _battleFinishCallback = callback;
            var battleOverPopup = Instantiate(_battleOverPopupRef, transform);
            battleOverPopup.Close += OnBattleOverPopupClosed;
            battleOverPopup.Show(_turret.Model.Score);
        }

        private void OnBattleOverPopupClosed(BattleOverPopup popup)
        {
            popup.Close -= OnBattleOverPopupClosed;
            popup.Remove();
            _battleFinishCallback?.Invoke();
        }

        private void OnTurretGetDamage(TurretModel turretModel)
        {
            _turretHealthField.text = turretModel.Health.ToString(CultureInfo.InvariantCulture);
        }
        
        private void OnTurretGetScore(TurretModel turretModel)
        {
            _turretScoreField.text = turretModel.Score.ToString(CultureInfo.InvariantCulture);
        }
    }
}