﻿using System;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SensoramaTest.Battle.UI
{
    public class BattleOverPopup : MonoBehaviour
    {
        public event Action<BattleOverPopup> Close;
        
        [SerializeField] private Button _restartButton;
        [SerializeField] private TextMeshProUGUI _scoreField;

        private void OnDestroy()
        {
            _restartButton.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            Close?.Invoke(this);
        }

        public void Show(float score)
        {
            _restartButton.onClick.AddListener(OnClick);
            _scoreField.text = $"Score: {score}";
        }

        public void Remove() => Destroy(gameObject);
    }
}