﻿using System;
using UnityEngine;

namespace SensoramaTest.Battle.UI
{
    public class UIToggler : IDisposable
    {
        private readonly UIController _rootCanvas;
        private readonly ProjectContext _context;

        public UIToggler(ProjectContext context)
        {
            _context = context;
            
            _rootCanvas = _context.BattleSceneContext.Ui;
            _rootCanvas.gameObject.SetActive(false);
            
            _context.ARStateController.StateChange += OnStateChange;
        }
        
        ~UIToggler() => Dispose();

        private void OnStateChange(ARStateController.State state)
        {
            _rootCanvas.gameObject.SetActive(state == ARStateController.State.Found);
        }

        public void Dispose()
        {
            _context.ARStateController.StateChange -= OnStateChange;
        }
    }
}