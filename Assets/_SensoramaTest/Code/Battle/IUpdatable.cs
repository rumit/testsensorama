﻿namespace SensoramaTest.Battle
{
    public interface IUpdatable
    {
        bool IsNeedUpdate { get; }
        void UpdateState();
    }
}