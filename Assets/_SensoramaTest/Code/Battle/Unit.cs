﻿using UnityEngine;

namespace SensoramaTest.Battle
{
    public abstract class Unit : MonoBehaviour, IUpdatable, ITarget
    {
        public bool IsNeedUpdate { get; protected set; }
        
        public abstract void GetDamage(float value);
        public abstract IUnitModel GetModel();

        public abstract bool IsDead { get; }
        public abstract void UpdateState();
        
        protected ProjectContext _context;
        protected BattleInputSystem _inputSystem;
        protected TargetsManager _targetsManager;
        protected Transform _transform;
    }
}