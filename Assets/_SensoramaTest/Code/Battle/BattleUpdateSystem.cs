﻿using System;
using System.Collections.Generic;
using SensoramaTest.Battle.Enemies;
using UnityEngine;

namespace SensoramaTest.Battle
{
    public class BattleUpdateSystem : MonoBehaviour
    {
        private readonly List<IUpdatable> _objects = new List<IUpdatable>();
        private float _minTargetDistance;
        private ProjectContext _context;
        private ARStateController _arStateObserver;
        
        private void OnDestroy()
        {
            _arStateObserver.StateChange -= OnStateChange;
            _objects.Clear();
        }

        public BattleUpdateSystem Begin(ProjectContext context)
        {
            _context = context;
            
            _arStateObserver = _context.ARStateController;
            _arStateObserver.StateChange += OnStateChange;
            return this;
        }
        
        public void Stop()
        {
            _objects.Clear();
        }

        private void OnStateChange(ARStateController.State state)
        {
            if(state == ARStateController.State.Lost)
                Remove(_context.BattleInputSystem);
            else
                Add(_context.BattleInputSystem);
        }

        public void Add(IUpdatable obj)
        {
            if (!_objects.Contains(obj))
                _objects.Add(obj);
        }

        public void Remove(IUpdatable obj)
        {
            if(_objects.Contains(obj))
                _objects.Remove(obj);
        }

        private void Update()
        {
            if(_objects.Count <= 0)
                return;
            
            for (var i = 0; i < _objects.Count; i++)
            {
                var o = _objects[i];
                if(o.IsNeedUpdate) o.UpdateState();
            }
        }
    }
}