﻿using SensoramaTest.Battle.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace SensoramaTest.Battle.Enemies
{
    public class Enemy : Unit
    {
        [SerializeField] private Image _hpBar;
        
        private EnemyConfig _config;

        private ITarget _target;
        private Vector3 _targetPosition;
        private float _minTargetDistance;

        private MovementComponent _movementComponent;
        private AttackComponent _attackComponent;
        
        public EnemyModel Model { get; private set; }

        public override bool IsDead => Model.Health <= 0;

        public void Init(ProjectContext context, EnemyConfig config, Vector3 position)
        {
            _context = context;
            _config = config;

            Model = new EnemyModel(config);
            IsNeedUpdate = true;

            _target = _context.BattleSceneContext.Turret;
            _targetPosition = _target.transform.position;

            Instantiate(config.View, Vector3.zero, Quaternion.identity, transform);
            InitComponents(position);

            var dist = _context.BattleConfig.MinTargetDistance;
            _minTargetDistance = dist * dist;
            
            _context.BattleSceneContext.TargetsManager.Add(this);
        }
        
        public override IUnitModel GetModel() => Model;

        private void InitComponents(Vector3 startPosition)
        {
            _movementComponent = new MovementComponent(transform, startPosition, _config.Speed);
            _attackComponent = new AttackComponent {Target = _target};
        }

        public override void UpdateState()
        {
            _movementComponent.MoveTo(_targetPosition);
            if((transform.position - _target.transform.position).sqrMagnitude >= _minTargetDistance)
                return;
                    
            Die();
            _context.BattleUpdateSystem.Remove(this);
            _attackComponent.Attack(_config.Damage);
        }

        private void Die()
        {
            IsNeedUpdate = false;
            var pos = transform.position + Vector3.up;
            Instantiate(_config.DieFXRef, pos, Quaternion.identity, _context.BattleSceneContext.BattleContainer.transform);
            _context.BattleSceneContext.TargetsManager.Remove(this);
        }

        public override void GetDamage(float value)
        {
            Model.SetDamage(value);
            _context.BattleSceneContext.TargetsManager.GetDamage?.Invoke(this);

            _hpBar.fillAmount = Model.Health / _config.Health;
            if (Model.Health <= 0)
                Die();
        }
    }
}