﻿using System;
using System.Collections;
using System.Collections.Generic;
using SensoramaTest.Battle.Configs;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using RangeInt = SensoramaTest.Battle.Configs.RangeInt;

namespace SensoramaTest.Battle.Enemies
{
    public class EnemiesSpawner : IDisposable
    {
        private readonly ProjectContext _context;
        private readonly Transform _battleRoot;
        private RangeFloat _spawnRangeInterval;

        private readonly ARStateController _arStateController;
        private float _spawnRadius;
        private RangeInt _rangeOfEnemiesNumber;
        private RangeFloat _distanceOffsetRange;

        private readonly float[] _angles = new float[20];
        
        private List<EnemyConfig> _enemyConfigs;
        private Enemy _enemyRef;

        private Coroutine _spawnCoroutine;

        public EnemiesSpawner(ProjectContext context)
        {
            _context = context;
            _arStateController = context.ARStateController;
            _battleRoot = context.BattleSceneContext.BattleContainer.transform;

            for (var i = 0; i < _angles.Length; i++) _angles[i] = i * 18;
            
            InitConfigSettings();
            AddListeners();
        }
        
        ~EnemiesSpawner() => Dispose();
        
        public void Dispose()
        {
            CoroutineHelper.CancelCoroutine(_spawnCoroutine);
            RemoveListeners();
        }

        public void Stop()
        {
            CoroutineHelper.CancelCoroutine(_spawnCoroutine);
        }

        private void InitConfigSettings()
        {
            _enemyConfigs = _context.BattleConfig.Enemies;
            _enemyRef = _context.BattleConfig.EnemyRef;
            
            var settings = _context.BattleConfig.SpawnSettings;
            _spawnRangeInterval = settings.TimeIntervalRange;
            _spawnRadius = settings.Radius;
            _distanceOffsetRange = settings.DistanceOffsetRange;
            _rangeOfEnemiesNumber = settings.RangeOfEnemiesNumber;
        }

        private void AddListeners()
        {
            _arStateController.StateChange += OnARStateChange;
        }
        
        private void RemoveListeners()
        {
            _arStateController.StateChange -= OnARStateChange;
        }
        
        private void OnARStateChange(ARStateController.State state)
        {
            if (state == ARStateController.State.Found)
                _spawnCoroutine = CoroutineHelper.LaunchCoroutine(SpawnNext());
            else
                CoroutineHelper.CancelCoroutine(_spawnCoroutine);
        }

        private IEnumerator SpawnNext()
        {
            InstantiateEnemies();
            
            while (true)
            {
                var time = Random.Range(_spawnRangeInterval.Min, _spawnRangeInterval.Max);
                yield return new WaitForSeconds(time);
                InstantiateEnemies();
            }
        }

        private void InstantiateEnemies()
        {
            var center = Vector2.zero;
            var count = GetEnemiesNumber();
            var angles = _angles.GetRandomElements(count);
            for (var i = 0; i < count; i++)
            {
                var config = _enemyConfigs[Random.Range(0, _enemyConfigs.Count)];
                if(_enemyRef == null)
                    continue;
                var enemy = Object.Instantiate(_enemyRef, _battleRoot);
                
                var tmp = GeomUtil.GetPointOnCircle(center, _spawnRadius, angles[i]);
                tmp = Vector2.Lerp(center, tmp, GetDistanceOffset());
                enemy.Init(_context, config, new Vector3(tmp.x, 0, tmp.y));
                _context.BattleUpdateSystem.Add(enemy);
            }
        }

        private int GetEnemiesNumber() => Random.Range(_rangeOfEnemiesNumber.Min, _rangeOfEnemiesNumber.Max);
        private float GetDistanceOffset() => Random.Range(_distanceOffsetRange.Min, _distanceOffsetRange.Max);
    }
}