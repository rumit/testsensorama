﻿namespace SensoramaTest.Battle.Enemies
{
    public class AttackComponent
    {
        public ITarget Target;
        
        public AttackComponent() { }

        public void Attack(float damage) => Target?.GetDamage(damage);
    }
}