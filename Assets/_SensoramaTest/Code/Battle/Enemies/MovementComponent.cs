﻿using UnityEngine;

namespace SensoramaTest.Battle.Enemies
{
    public class MovementComponent
    {
        private readonly Transform _transform;
        private readonly float _speed;

        public MovementComponent(Transform ownerTransform, Vector3 position, float speed)
        {
            _transform = ownerTransform;
            _transform.position = position;
            _speed = speed;
            _transform.LookAt(Vector3.zero);
        }

        public void MoveTo(Vector3 targetPosition)
        {
            var step =  _speed * Time.deltaTime;
            _transform.LookAt(targetPosition);
            _transform.position = Vector3.MoveTowards(_transform.position, targetPosition, step);
        }
    }
}