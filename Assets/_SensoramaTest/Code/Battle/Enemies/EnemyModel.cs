﻿using System;
using SensoramaTest.Battle.Configs;
using UnityEngine;

namespace SensoramaTest.Battle.Enemies
{
    [Serializable]
    public class EnemyModel : IUnitModel
    {
        private readonly EnemyConfig _config;

        public float Health { get; private set; }
        
        public float MaxHealth => _config.Health;
        
        public EnemyModel(EnemyConfig config)
        {
            _config = config;
            Health = config.Health;
        }
    
        public void SetDamage(float value)
        {
            Health -= value;
            Health = Mathf.Clamp(Health, 0, _config.Health);
        }
    }
}