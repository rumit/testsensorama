﻿using System;
using System.Collections.Generic;
using SensoramaTest.Battle.Enemies;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SensoramaTest.Battle
{
    public class TargetsManager
    {
        public Action<ITarget> GetDamage;
        
        public readonly List<ITarget> Targets = new List<ITarget>();

        public TargetsManager() { }

        public void Add(Enemy enemy)
        {
            if (!Targets.Contains(enemy))
                Targets.Add(enemy);
        }
        
        public void Remove(Enemy enemy)
        {
            if (Targets.Contains(enemy))
            {
                Targets.Remove(enemy);
                Object.Destroy(enemy.gameObject);
            }
        }

        public void RemoveAll()
        {
            foreach (var enemy in Targets)
                Object.Destroy(enemy.gameObject);
            Targets.Clear();
        }
    }
}