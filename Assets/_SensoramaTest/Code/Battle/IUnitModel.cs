﻿namespace SensoramaTest.Battle
{
    public interface IUnitModel
    {
        float MaxHealth { get; }
    }
}