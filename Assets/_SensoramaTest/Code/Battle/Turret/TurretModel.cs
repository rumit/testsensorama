﻿using System;
using SensoramaTest.Battle.Configs;
using UnityEngine;

namespace SensoramaTest.Battle.Turret
{
    [Serializable]
    public class TurretModel : IUnitModel
    {
        public event Action<TurretModel> GetDamage;
        public event Action<TurretModel> GetScore;
        
        public float Health { get; private set; }
        public float Score { get; private set; }
        
        public float MaxHealth => _config.Health;
        
        private TurretConfig _config;

        public TurretModel(TurretConfig config)
        {
            _config = config;
            Health = config.Health;
        }

        public void Reset()
        {
            Health = _config.Health;
            Score = 0;
        }

        public void SetDamage(float value)
        {
            Health -= value;
            Health = Mathf.Clamp(Health, 0, _config.Health);
            GetDamage?.Invoke(this);
        }
        
        public void SetScore(float value)
        {
            Score += value;
            //Debug.Log($"SetScore: {value}");
            GetScore?.Invoke(this);
        }
    }
}