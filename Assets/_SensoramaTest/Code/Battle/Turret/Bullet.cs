﻿using System;
using UnityEngine;

namespace SensoramaTest.Battle.Turret
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : MonoBehaviour, IUpdatable
    {
        [SerializeField] private ParticleSystem _explosionRef;
        [SerializeField] private TrailRenderer _trail;
        
        private Rigidbody _rigidbody;
        private Action<Bullet, Vector3> _completeCallback;
        
        public bool IsNeedUpdate { get; private set; }

        public void Init(Vector3 velocity, Action<Bullet, Vector3> callback)
        {
            IsNeedUpdate = true;
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.velocity = velocity;
            _completeCallback = callback;
        }
        
        public void UpdateState()
        {
            if (transform.position.y <= 0.1f)
                Explode();
        }

        private void Explode()
        {
            Instantiate(_explosionRef, transform.position + Vector3.up * 1.2f, Quaternion.identity, transform.parent);
            
            _trail.Clear();
            IsNeedUpdate = false;
            _completeCallback?.Invoke(this, transform.position);
        }
    }
}