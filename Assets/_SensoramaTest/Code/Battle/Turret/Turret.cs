﻿using SensoramaTest.Battle.Configs;
using SensoramaTest.Battle.Enemies;
using UnityEngine;

namespace SensoramaTest.Battle.Turret
{
    public class Turret : Unit
    {
        [SerializeField] private TurretConfig _config;
        [SerializeField] private Transform _rotatable;
        [SerializeField] private Cannon _cannon;

        public TurretModel Model { get; private set; }
        
        public override bool IsDead => Model.Health <= 0;

        private AttackComponent _attackComponent;

        private Vector3 _targetPoint;
        private float _rotationSpeed;
        private float _blindAreaRadiusSqrt;

        private void OnDestroy()
        {
            RemoveListeners();
            _context.BattleUpdateSystem.Remove(this);
        }

        public void Init(ProjectContext context)
        {
            Model = new TurretModel(_config);
            IsNeedUpdate = true;
            
            _context = context;
            _transform = transform;
            _rotationSpeed = _config.RotationSpeed;
            
            _targetsManager = _context.BattleSceneContext.TargetsManager;
            
            var blindAreaRadius = _config.Attack.BlindAreaRadius;
            _blindAreaRadiusSqrt =  blindAreaRadius * blindAreaRadius;
            
            _inputSystem = context.BattleInputSystem;
            _inputSystem.Tapped += OnWorldPlaneTap;
            
            _cannon.Init(_config, context);
            _attackComponent = new AttackComponent(_context.BattleSceneContext.TargetsManager);

            AddListeners();
        }

        private void AddListeners()
        {
            _targetsManager.GetDamage += OnTargetGetDamage;
            _cannon.CompleteShot += OnCompleteShot;
        }
        
        private void RemoveListeners()
        {
            _targetsManager.GetDamage -= OnTargetGetDamage;
            _cannon.CompleteShot -= OnCompleteShot;
        }

        private void OnTargetGetDamage(ITarget target)
        {
            if(!(target is Enemy))
                return;
            
            if (target.IsDead)
                Model.SetScore(target.GetModel().MaxHealth);
        }

        public void Reset()
        {
            gameObject.SetActive(true);
            Model.Reset();
            IsNeedUpdate = true;
            _rotatable.localRotation = Quaternion.identity;
        }

        public override void GetDamage(float value) => Model.SetDamage(value);
        public override IUnitModel GetModel() => Model;

        public void Destroy()
        {
            gameObject.SetActive(false);
            IsNeedUpdate = false;
            Instantiate(_config.ExplosionRef, transform.position, Quaternion.identity,
                _context.BattleSceneContext.BattleContainer.transform);
        }

        public override void UpdateState()
        {
            var targetDir = _targetPoint - _rotatable.position;
            var dir = Vector3.RotateTowards(_rotatable.forward, targetDir, _rotationSpeed * Time.deltaTime, 0);
            _rotatable.rotation = Quaternion.LookRotation(dir, Vector3.up);

            if (Vector3.Angle(_rotatable.forward, targetDir) <= 0.01f)
            {
                _context.BattleUpdateSystem.Remove(this);
                var targetPoint = new Vector3(_targetPoint.x, _transform.position.y, _targetPoint.z);
                _cannon.Fire(targetPoint);
            }
        }
        
        private void OnWorldPlaneTap(Vector3 point)
        {
            if ((_transform.position - point).sqrMagnitude <= _blindAreaRadiusSqrt)
                return;

            _targetPoint = point;
            _context.BattleUpdateSystem.Add(this);
        }
        
        private void OnCompleteShot(Vector3 point)
        {
            _attackComponent.Attack(_config.Attack, point);
        }
    }
}