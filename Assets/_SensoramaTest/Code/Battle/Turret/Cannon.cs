﻿using System;
using System.Collections.Generic;
using SensoramaTest.Battle.Configs;
using Unity.Mathematics;
using UnityEngine;

namespace SensoramaTest.Battle.Turret
{
    public class Cannon : MonoBehaviour
    {
        public event Action<Vector3> CompleteShot;
        
        [SerializeField] private ParticleSystem _smokeFX;

        private readonly float _gravity = Physics.gravity.y;

        private ProjectContext _context;
        
        private float _angle;
        private Bullet _bulletRef;
        private Transform _transform;
        private Transform _rootTransform;

        private const int _poolCapacity = 50;
        private readonly Stack<Bullet> _bulletsPool = new Stack<Bullet>(_poolCapacity);

        public void Init(TurretConfig config, ProjectContext context)
        {
            _transform = transform;
            _context = context;
            _rootTransform = context.BattleSceneContext.BattleContainer.transform;
            _bulletRef = config.Attack.BulletRef;
            
            for (var i = 0; i < _poolCapacity; i++)
            {
                var bullet = Instantiate(_bulletRef, _transform.position, Quaternion.identity, _rootTransform);
                PushBullet(bullet);
            }
        }

        public void Fire(Vector3 target)
        {
            _angle = _transform.localEulerAngles.x;
            
            var fromTo = target - _transform.position;;
            var x = fromTo.magnitude;
            var y = fromTo.y;
            var angleRad = _angle * Mathf.Deg2Rad;
                
            var v2 = _gravity * x * x / (2 * (y - Mathf.Tan(angleRad) * x) * Mathf.Pow(Mathf.Cos(angleRad), 2));
            var v = Mathf.Sqrt(Mathf.Abs(v2));
            
            _smokeFX.Play();

            var bullet = PopBullet();
            _context.BattleUpdateSystem.Add(bullet);
            bullet.Init(_transform.forward * v, OnCompleteShot);
        }
        
        private void OnCompleteShot(Bullet bullet, Vector3 point)
        {
            _context.BattleUpdateSystem.Remove(bullet);
            PushBullet(bullet);
            CompleteShot?.Invoke(point);
        }

        private Bullet PopBullet()
        {
            var bullet = _bulletsPool.Pop();
            bullet.gameObject.SetActive(true);
            bullet.transform.position = _transform.position;
            return bullet;
        }
        
        private void PushBullet(Bullet bullet)
        {
            bullet.gameObject.SetActive(false);
            _bulletsPool.Push(bullet);
        }
    }
}