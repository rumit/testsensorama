﻿using System.Collections.Generic;
using System.Linq;
using SensoramaTest.Battle.Configs;
using SensoramaTest.Battle.Enemies;
using UnityEngine;

namespace SensoramaTest.Battle.Turret
{
    public class AttackComponent
    {
        private readonly TargetsManager _targetsManager;

        public AttackComponent(TargetsManager targetsManager)
        {
            _targetsManager = targetsManager;
        }
        
        public void Attack(TurretConfig.AttackData data, Vector3 attackCenter)
        {
            var radius = data.Radius;
            for (var i = 0; i < _targetsManager.Targets.Count; i++)
            {
                var e = _targetsManager.Targets[i];
                var d = Vector3.Distance(e.transform.position, attackCenter);
                if (d > radius)
                    continue;

                var damage = data.Damage * (d / radius);
                e.GetDamage(damage);
            }
        }
    }
}