﻿using System;
using System.Collections;
using SensoramaTest.Battle.Enemies;
using SensoramaTest.Battle.Turret;
using SensoramaTest.Battle.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SensoramaTest.Battle
{
    public class BattleState : IDisposable
    {
        private readonly ProjectContext _context;
        private readonly Turret.Turret _turret;
        private readonly UIController _uiController;
        
        private readonly BattleUpdateSystem _battleUpdateSystem;
        private readonly EnemiesSpawner _enemiesSpawner;
        private readonly TargetsManager _enemiesManager;

        public BattleState(ProjectContext context)
        {
            _context = context;
            _uiController = _context.BattleSceneContext.Ui;
            _turret = _context.BattleSceneContext.Turret;
            
            _battleUpdateSystem = _context.BattleUpdateSystem;
            _enemiesSpawner = _context.BattleSceneContext.EnemiesSpawner;
            _enemiesManager = _context.BattleSceneContext.TargetsManager;

            _turret.Model.GetDamage += OnTurretGetDamage;
        }
        
        ~BattleState() => Dispose();

        private void OnTurretGetDamage(TurretModel turretModel)
        {
            if (turretModel.Health <= 0)
                CoroutineHelper.LaunchCoroutine(SetBattleOver());
        }

        private IEnumerator SetBattleOver()
        { 
            _turret.Destroy();
            //_context.ARStateController.Stop();
            _battleUpdateSystem.Stop();
            _enemiesSpawner.Stop();
            _enemiesManager.RemoveAll();
            
            yield return new WaitForSeconds(2);

            _uiController.DisplayBattleOver(() =>
            {
                _turret.Reset();
                _uiController.ResetTurretData();
                _context.ARStateController.TempRestart();
            });
        }

        public void Dispose()
        {
            _turret.Model.GetDamage -= OnTurretGetDamage;
        }
    }
}