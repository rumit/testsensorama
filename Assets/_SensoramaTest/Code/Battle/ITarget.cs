﻿using UnityEngine;

namespace SensoramaTest.Battle
{
    public interface ITarget
    {
        void GetDamage(float value);
        IUnitModel GetModel();
        bool IsDead { get; }
        Transform transform { get; }
        GameObject gameObject { get; }
    }
}