﻿using System;
using SensoramaTest.Battle;
using SensoramaTest.Battle.Enemies;
using SensoramaTest.Battle.UI;
using UnityEngine;

namespace SensoramaTest.Battle
{
    public class BattleInitializer
    {
        public BattleInitializer(ProjectContext context)
        {
            var battleSceneContext = context.BattleSceneContext;
            battleSceneContext.EnemiesSpawner = new EnemiesSpawner(context);
            battleSceneContext.TargetsManager = new TargetsManager();
            
            var turret = battleSceneContext.Turret;
            turret.Init(context);

            battleSceneContext.Ui.Init(context);

            new UIToggler(context);
            new BattleState(context);
        }
    }
}