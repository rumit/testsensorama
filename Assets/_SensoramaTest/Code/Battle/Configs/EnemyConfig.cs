﻿using SensoramaTest.Battle.Enemies;
using UnityEngine;

namespace SensoramaTest.Battle.Configs
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "SensoramaTest/EnemyConfig", order = 0)]
    public class EnemyConfig : ScriptableObject
    {
        [SerializeField] private float _health;
        [SerializeField] private float _speed;
        [SerializeField] private float _damage;
        [SerializeField] private FBXModel _view;
        [SerializeField] private ParticleSystem _dieFXRef;

        public float Health => _health;
        public float Speed => _speed;
        public float Damage => _damage;
        public FBXModel View => _view;
        public ParticleSystem DieFXRef => _dieFXRef;
    }
}