﻿using System;
using SensoramaTest.Battle.Enemies;
using SensoramaTest.Battle.Turret;
using UnityEngine;

namespace SensoramaTest.Battle.Configs
{
    [CreateAssetMenu(fileName = "TurretConfig", menuName = "SensoramaTest/TurretConfig", order = 0)]
    public class TurretConfig : ScriptableObject
    {
        [SerializeField] private float _health;
        [SerializeField] private float _rotationSpeed = 1f;
        [SerializeField] private ParticleSystem _explosionRef;
        [Space(5)]
        [SerializeField] private AttackData _attack;
        
        public ParticleSystem ExplosionRef => _explosionRef;

        public float Health => _health;
        public AttackData Attack => _attack;
        public float RotationSpeed => _rotationSpeed;

        [Serializable]
        public struct AttackData
        {
            public float BlindAreaRadius;
            public float Radius;
            public float Damage;
            public Bullet BulletRef;
        }
    }
}