﻿using System;
using System.Collections.Generic;
using SensoramaTest.Battle.Enemies;
using SensoramaTest.Battle.UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace SensoramaTest.Battle.Configs
{
    [CreateAssetMenu(fileName = "BattleConfig", menuName = "SensoramaTest/BattleConfig", order = 0)]
    public class BattleConfig : ScriptableObject
    {
        [SerializeField] private SpawnSettings _spawnSettings;
        public SpawnSettings SpawnSettings => _spawnSettings;
        
        [Space(20)]
        [SerializeField] private float _minTargetDistance;
        public float MinTargetDistance => _minTargetDistance;

        [Space(20)]
        [SerializeField] private Enemy _enemyPrefab;
        public Enemy EnemyRef => _enemyPrefab;
        
        [Space(5)]
        [SerializeField] private List<EnemyConfig> _enemies;
        public List<EnemyConfig> Enemies => _enemies;
        
        [Space(5)]
        [SerializeField] private BattleOverPopup _battleOverPopupRef;
        public BattleOverPopup BattleOverPopuRef => _battleOverPopupRef;
    }

    [Serializable]
    public struct SpawnSettings
    {
        public float Radius;
        [Tooltip("percentage of radius")]
        public RangeFloat DistanceOffsetRange;
        [Space(5)]
        public RangeFloat TimeIntervalRange;
        [Space(5)]
        public RangeInt RangeOfEnemiesNumber;
    }
    
    [Serializable]
    public struct RangeInt
    {
        public int Min;
        public int Max;
    }
    
    [Serializable]
    public struct RangeFloat
    {
        public float Min;
        public float Max;
    }
}