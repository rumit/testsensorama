﻿using System;
using UnityEngine;

namespace SensoramaTest.Battle
{
    public class BattleInputSystem : MonoBehaviour, IUpdatable
    {
        public bool IsNeedUpdate { get; } = true;
        
        public event Action<Vector3> Tapped;
        private Camera _camera;
        private Plane _ground;

        public BattleInputSystem Init(ProjectContext context)
        {
            _camera = context.BattleSceneContext.Camera;
            if (_camera == null)
                _camera = Camera.main;
            
            _ground = new Plane(Vector3.up, Vector3.zero);
            return this;
        }

        public void UpdateState()
        {
            if (!Input.GetMouseButtonUp(0)) 
                return;
            
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (!_ground.Raycast(ray, out var pos)) 
                return;
            
            var wp = ray.GetPoint(pos);
            Tapped?.Invoke(wp);
        }
    }
}