﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SensoramaTest
{
    public class ARStateController : IDisposable
    {
        public enum State
        {
            Found,
            Lost
        }
        public event Action<State> StateChange;

        private readonly DefaultObserverEventHandler _observer;

        public ARStateController(ProjectContext context)
        {
            _observer = context.BattleSceneContext.ArObserver;
            AddListeners();
            //CoroutineHelper.LaunchCoroutine(TargetFoundDebugCall()); ///TEMP
        }

        ///TEMP
        public void TempRestart()
        {
            CoroutineHelper.LaunchCoroutine(TargetFoundDebugCall());
        }
        
        ~ARStateController() => Dispose();

        public void Dispose()
        {
            RemoveListeners();
        }

        private IEnumerator TargetFoundDebugCall()
        {
            yield return new WaitForSeconds(3);
            OnTargetFound();
        }

        public void Stop() => RemoveListeners();
        
        public void Restart() => AddListeners();

        private void AddListeners()
        {
            _observer.OnTargetFound.AddListener(OnTargetFound);
            _observer.OnTargetLost.AddListener(OnTargetLost);
        }
        
        private void RemoveListeners()
        {
            _observer.OnTargetFound.RemoveListener(OnTargetFound);
            _observer.OnTargetLost.RemoveListener(OnTargetLost);
        }
        
        private void OnTargetFound()
        {
            StateChange?.Invoke(State.Found);
            Debug.Log($"OnTargetFound");
        }
        
        private void OnTargetLost()
        {
            StateChange?.Invoke(State.Lost);
            Debug.Log($"OnTargetLost");
        }
    }
}